import numpy
import scipy.sparse.linalg

def get_gs(H,num_gs=1):
    try:
        return scipy.sparse.linalg.eigsh(H,k=num_gs,which='SA') # = (E_gs,gs)
    except Exception as error:
        sys.stderr.write('Issues with convergence in function get_gs...\n')
        sys.stderr.write(str(error)+'\n')
        return None
        
        
