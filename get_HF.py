import numpy
import numpy.linalg
import scipy.integrate as integ
from mk_qm import *

# Get the Floquet Hamiltonian for a Hamiltonian ramping from t=0 to t=T
# Slight modification from previous code: assume H_fn gives a sparse matrix
def get_HF(H_fn, T, n_t=100):
    basis_sz=H_fn(0.).shape[0]
    #    print 'basis_sz='+str(basis_sz)
    basis_sz_sq=basis_sz**2

    my_ode=integ.ode(
        lambda t, y: -1.j*H_fn(t).dot(y) #, jac=(lambda t, y: -1.j*H_fn(t))
        ).set_integrator('zvode',method='adams',atol=1e-12,rtol=1e-12)
    # my_ode=integ.ode(
    #     lambda t, y: -1.j*numpy.dot(H_fn(t),y.reshape((basis_sz,basis_sz))).reshape((basis_sz_sq)),
    #     ).set_integrator('zvode',method='bdf')

    dt=T/n_t

    U_cycle=numpy.zeros((basis_sz,basis_sz),dtype=complex)
    psi=U_cycle[:,0]+0.

    j=0
    while j < basis_sz:
        print('j='+str(j))
        psi[j-1]=0.
        psi[j]=1.
        my_ode.set_initial_value(psi,0.)
        while my_ode.successful() and my_ode.t < T-dt/2.:
            my_ode.integrate(my_ode.t+dt)
        #            print 'my_ode.t='+str(my_ode.t)
        U_cycle[:,j]=my_ode.y+0.
        j+=1

    #    U_cycle=+0.
    #    U_cycle=my_ode.y.reshape((basis_sz,basis_sz))

        
    # U_cycle=reshape(my_ode.y) will be diagonalized by U_c_d = U_d^{-1} U_c U_d
    # if not is_unitary(U_cycle):
    #     print 'U_cycle is not unitary'
    #     print r'U_cycle*U_cycle^\dagger='+str(U_cycle.dot(adjoint(U_cycle)))
    #     import sys
    #     sys.exit()
    (U_cycle_diag_unsrt,U_diag_unsrt)=eigu(U_cycle)

    # diff=U_cycle-numpy.dot(U_diag_unsrt,numpy.dot(numpy.diag(U_cycle_diag_unsrt),adjoint(U_diag_unsrt)))
    # print 'Max difference of U before sorting in get_HF is '+str(max(numpy.abs(diff.flatten())))

    H_F_diag=numpy.real(1.j*numpy.log(U_cycle_diag_unsrt)/T)
    inds=numpy.argsort(H_F_diag)
    H_F_diag=H_F_diag[inds]

    # Make U_diag unitary while reshuffling indices
    U_diag=0.*U_diag_unsrt
    # for j in range(basis_sz):
    #     V[:,j]=U[:,inds[j]]
    for j in range(U_diag.shape[1]):
        U_diag[:,j]=U_diag_unsrt[:,inds[j]]/norm_sq(U_diag_unsrt[:,inds[j]])**0.5

        #    assert is_unitary(U_diag)
        
    U_cycle_diag=U_cycle_diag_unsrt[inds]
    # diff=U_cycle-numpy.dot(U_diag,numpy.dot(numpy.diag(U_cycle_diag),adjoint(U_diag)))
    # print 'Max difference of U after sorting in get_HF is '+str(max(numpy.abs(diff.flatten())))

    H_F=numpy.dot(U_diag,numpy.dot(numpy.diag(H_F_diag),U_diag.conjugate().transpose()))

    #    return (H_F, H_F_diag, U_diag, U_cycle)
    return (H_F, H_F_diag, U_diag)

