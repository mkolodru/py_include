import curve_fit
import numpy

def constfit(x, b):
	return b

def slopefit(x, m, b):
	return m*x

def linearfit(x, m, b):
	return m*x+b

def quadrfit(x,a,b,c):
	return a*x**2+b*x+c

def cubicfit(x,a,b,c,d):
	return a*x**3+b*x**2+c*x+d

def powerfit(x, A, B):
	return A*(x**B)

def expfit(x, A, B, C):
	return A+B*numpy.exp(C*x)

def decayfit(x, A, B):
	return A*numpy.exp(B*x)

def constr_decayfit(x, gamma):
	return numpy.exp(-gamma*x)

def biexpfit(x, A, B, C, D, E):
	return A+B*numpy.exp(C*x)+D*numpy.exp(E*x)

def decayoscfit(x, A, B, C, D, E):
	return A*numpy.exp(B*x)*numpy.cos(C*x+D)+E

def oscfit(x, A, B, C, D):
	return A*numpy.cos(B*x+C)+D

def phasefit(x, A, B):
	return A*x*numpy.exp(-B/(2.*x**2))

def lor(x, A, g, w):
	return A*g/(numpy.pi*((x-w)**2+g**2))

def bilorfit(x, A1, g1, A2, g2, w1, w2):
	return lor(x,A1,g1,w1)+lor(x,A2,g2,w2)

def gaussian(x, A, x_0, s_x):
	return A*numpy.exp(-(x-x_0)**2/(2.*s_x**2))

def dofit(fn,x,y,sigma=None,p0=None,**kw):
	(popt,pcov)=curve_fit.curve_fit(fn,x,y,sigma=sigma,p0=p0,**kw)
	sopt=[]
	for j in range(len(popt)):
		if not isinstance(pcov,numpy.ndarray):
			sopt.append(numpy.nan)
		else:
			sopt.append(pcov[j,j]**0.5)
	return (popt,sopt)

def print_fit(fn,param,sparam,fout):
	pvals=getprintvals(fn)
	fout.write('Fitting function: '+pvals[0]+'\n')
	for j in range(len(param)):
		fout.write('\t'+pvals[1][j]+' = '+str(param[j])+' +- '+str(sparam[j])+'\n')

def getprintvals(fn):
	function_def='Undefined'
	var_list=[]
	if fn == linearfit:
		function_def='y=m*x+b'
		var_list=['m','b']
	if fn == slopefit:
		function_def='y=m*x'
		var_list=['m','b']
	if fn == quadrfit:
		function_def='y=ax^2+bx+c'
		var_list=['a','b','c']
	if fn == cubicfit:
		function_def='y=ax^3+bx^2+cx+d'
		var_list=['a','b','c','d']
	if fn == expfit:
		function_def='y=A+B*exp(C*x)'
		var_list=['A','B','C']
	if fn == biexpfit:
		function_def='y=A+B*exp(C*x)+D*exp(E*x)'
		var_list=['A','B','C','D','E'] 
	if fn == decayoscfit:
		function_def='y=A*exp(B*x)*cos(C*x+D)+E'
		var_list=['A','B','C','D','E']
	if fn == oscfit:
		function_def='y=A*cos(B*x+C)+D'
		var_list=['A','B','C','D']
	if fn == decayfit:
		function_def='y=A*exp(B*x)'
		var_list=['A','B']
	if fn == constr_decayfit:
		function_def='y=exp(-Gamma*x)'
		var_list=['Gamma']
	if fn == powerfit:
		function_def='y=A*x^B'
		var_list=['A','B']
	if fn == phasefit:
		function_def='y=A*x*exp(-B/(2 x^2))'
		var_list=['A','B']
	if fn == bilorfit:
		function_def='y=lor_(A1,g1,w1)[x]+lor_(A2,g2,w2)[x]'
		var_lst=['A1','g1','A2','g2']
	if fn == gaussian:
		function_def='y=A*exp(-(x-x_0)^2/2 s_x^2)'
		var_lst=['A','x_0','s_x']
	return [function_def,var_list]
