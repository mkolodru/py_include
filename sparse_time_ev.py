import numpy
import numpy.linalg
import scipy.sparse
import scipy.linalg
from mk_qm import *

class sparse_time_ev:
    '''Class implementing time evolution under a fixed sparse matrix'''
    
    def __init__(self,H,psi,m=5):
        self.H=H.tocsc()
        self.psi=psi+0.
        self.m=m # m = size of Krylov subspace or order of Pade expansion
        self.K=numpy.zeros((len(psi),m),dtype=complex)

    def evolve_krylov(self,dt):
        '''Evolve psi to exp(-i H dt)*psi using Krylov methods.'''

        self.K[:,0]=self.psi
        sz=self.K.shape[1]+0
        for j in range(1,self.m):
            #            print 'At j='+str(j)+', K='+str(self.K)
            self.K[:,j]=self.H.dot(self.K[:,j-1])
            try:
                self.K[:,j]=normalize(self.K[:,j]-self.K[:,:j].dot(adjoint(self.K[:,:j]).dot(self.K[:,j])))
            except ValueError:
                # print 'Raised ValueError'
                # print 'self.H.dot(self.K[:,j-1]) = '+str(self.H.dot(self.K[:,j-1]))
                # self.K[:,j]=self.H.dot(self.K[:,j-1])
                # print 'self.K[:,j] = '+str(self.K[:,j])
                # print 'self.K[:,:j].dot(adjoint(self.K[:,:j]).dot(self.K[:,j])) = '+str(self.K[:,:j].dot(adjoint(self.K[:,:j]).dot(self.K[:,j])))
                #                print 'numpy.sum(self.K[:,:j].dot(adjoint(self.K[:,:j]).dot(self.K[:,j]),axis=1) = '+str(numpy.sum(self.K[:,:j].dot(adjoint(self.K[:,:j]).dot(self.K[:,j])).reshape(self.K.shape[0],j),axis=1))
                sz=j+0
                break
        # print 'At end, K='+str(self.K)
        # print 'sz='+str(sz)
        #        self.K=scipy.linalg.orth(self.K) # Might be able to do this more efficiently by orthog as I go.
        psi_eff=adjoint(self.K[:,:sz]).dot(self.psi)
        H_eff=adjoint(self.K[:,:sz]).dot(self.H.dot(self.K[:,:sz]))
        (E_eff,V_eff)=numpy.linalg.eigh(H_eff) # H_eff = V_eff * E_eff * V_eff^dagger
        psi_eff=V_eff.dot(numpy.exp(-1.j*E_eff*dt)*(adjoint(V_eff).dot(psi_eff)))
        self.psi=self.K[:,:sz].dot(psi_eff)

        return self.psi

    def evolve_pade(self,dt):
        '''Evolve psi to exp(-i H dt)*psi using m-th order Pade approx.'''

        psi2=-1.j*dt*self.H.dot(self.psi)
        self.psi=self.psi+psi2
        for k in range(2,self.m):
            psi2=-1.j*dt*self.H.dot(psi2)/float(k)
            self.psi=self.psi+psi2
        normalize(self.psi)
        
        return self.psi
