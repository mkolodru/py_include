import numpy
import scipy
import scipy.misc

def euler_inversion(f_s, t, M=32):

    # ilt = euler_inversion(f_s, t, [M])
    #
    # Returns an approximation to the inverse Laplace transform of function
    # handle f_s evaluated at each value in t (1xn) using the Euler method as
    # summarized in the source below.
    # 
    # This implementation is very coarse; use euler_inversion_sym for better
    # precision. Further, please see example_inversions.m for examples.
    #
    # f_s: Handle to function of s
    # t:   Times at which to evaluate the inverse Laplace transformation of f_s
    # M:   Optional, number of terms to sum for each t (64 is a good guess);
    #      highly oscillatory functions require higher M, but this can grow
    #      unstable; see test_talbot.m for an example of stability.
    # 
    # Abate, Joseph, and Ward Whitt. "A Unified Framework for Numerically 
    # Inverting Laplace Transforms." INFORMS Journal of Computing, vol. 18.4 
    # (2006): 408-421. Print.
    # 
    # The paper is also online: http://www.columbia.edu/~ww2040/allpapers.html.
    # 
    # Tucker McClure
    # Copyright 2012, The MathWorks, Inc.
    
    # Make sure t is n-by-1.
    if len(t.shape)!=1:
        sys.stderr.write('Input times, t, must be a vector\n')
        sys.exit()

    # Vectorized Euler's algorithm
    #    bnml = @(n, z) prod((n-(z-(1:z)))./(1:z)); # binomial coefficient

    xi=numpy.array([0.5])
    xi=numpy.append(xi,numpy.ones(M))
    xi=numpy.append(xi,numpy.zeros(M-1))
    xi=numpy.append(xi,numpy.array([2.**(-M)]))
    for k in range(1,M):
        xi[2*M-k]=xi[2*M-k+1]+2.**(-M)*scipy.misc.comb(M,k)

    k=numpy.arange(2*M+1,dtype=int)
    beta=float(M)*numpy.log(10.)/3.+1.j*numpy.pi*k
    eta=(1.-numpy.mod(k,2)*2.)*xi
    
    #    k = 0:2*M; # Iteration index
    #    beta = M*log(10)/3 + 1i*pi*k;
    #    eta  = (1-mod(k, 2)*2) .* xi;
    
    # Make a mesh so we can do this entire calculation across all k for all
    # given times without a single loop (it's faster this way).
    (beta_mesh, t_mesh) = numpy.meshgrid(beta, t)
    (eta_mesh, t_mesh) = numpy.meshgrid(eta, t)
    
    # Finally, calculate the inverse Laplace transform for each given time.
    ilt=numpy.zeros((len(t)))
    assert len(t)==t_mesh.shape[0]
    j=0
    while j < t_mesh.shape[0]:
        l=0
        while l < t_mesh.shape[1]:
            ilt[j]+=eta_mesh[j,l]*numpy.real(f_s(beta_mesh[j,l]/t_mesh[j,l]))
            l+=1
        ilt[j]*=(10.**(float(M)/3.)/t[j])
        j+=1
    return ilt

