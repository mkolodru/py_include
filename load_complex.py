import numpy

def get_matrix(fin):
	line=fin.readline()
	if len(line) == 0:
		return None
	sz=int(line)
	MM=numpy.zeros((sz,sz),dtype=complex)
	k=0
	while k < sz:
		pts=fin.readline().split()
		assert len(pts)==sz
		j=0
		while j < sz:
			pts2=pts[j][1:-1].split(',')
			MM[k,j]=complex(float(pts2[0]),float(pts2[1]))
			j+=1
		k+=1

	return MM

def get_matrix_block(fin,beginind,endind):
	line=fin.readline()
	if len(line) == 0:
		return None
	sz=int(line)
	if endind==-1 or endind > sz:
		endind=sz
	block_sz=endind-beginind
	MM=numpy.zeros((block_sz,block_sz),dtype=complex)
	k=0
	while k < endind:
		pts=fin.readline().split()
		if k < beginind:
			k+=1
			continue
		assert len(pts)==sz
		j=beginind
		while j < endind:
			pts2=pts[j][1:-1].split(',')
			MM[k-beginind,j-beginind]=complex(float(pts2[0]),float(pts2[1]))
			j+=1
		k+=1

	return MM

def load_complex(infile):
	fin=open(infile,'r')
	return get_matrix(fin)
