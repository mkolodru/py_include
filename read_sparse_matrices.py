import numpy

def load_complex_sparse_matrix(f):
    sz=int(open(f,'r').readline())
    vals=numpy.loadtxt(f,skiprows=1)
    M=numpy.zeros((sz,sz),dtype=complex)
    for row in range(vals.shape[0]):
        M[int(vals[row,0]),int(vals[row,1])]=complex(vals[row,2],vals[row,3])
    return M

def load_real_sparse_matrix(f):
    sz=int(open(f,'r').readline())
    vals=numpy.loadtxt(f,skiprows=1)
    M=numpy.zeros((sz,sz))
    for row in range(vals.shape[0]):
        M[int(vals[row,0]),int(vals[row,1])]=vals[row,2]
    return M

