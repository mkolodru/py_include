import numpy
import sys
import glob

if len(sys.argv) < 2:
	print("Proper usage: extract_corrtimes.py $variable [$outfile] [$REAL_TIME=(Y)/N]")
	assert 1==2

varname=sys.argv[1]
infile=varname+'_stats.dat'
outfile=varname+'_corr_stats.dat'
if len(sys.argv)>2:
	outfile=sys.argv[2]
realT=True
if len(sys.argv)>3 and sys.argv[3].find('N')!=-1:
	realT=False
out=open(outfile,'w')
for line in open(infile,'r'):
	pts=line.split()
	dt=1
	if realT:
		dtfile=glob.glob('v'+pts[0]+'/build/dt_*.out')[0]
		dt=numpy.loadtxt(dtfile)[1]
	out.write(pts[0]+'\t'+str(float(pts[4])*dt)+'\t0\t0\t0\n')
