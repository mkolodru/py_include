import numpy
import scipy.sparse

def get_z(u,site):
    return 1- 2 * ((u >> site) % 2)

def flip_site(u,site):
    return u ^ (1 << site)

        
