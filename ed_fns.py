import numpy
import scipy.sparse.linalg

def get_n_up(state,L):
    numups=0
    j=0
    while j < L:
        numups+=1-(state>>j)%2
        j+=1
    return numups
    
def getZ(state, site):
    return 1.-2.*((state>>site)%2)

def getZZ(state, site, L, step=1):
    return 1. if (state>>site)%2==(state>>((site+step)%L))%2 else -1.

def flip_neighbors(state, site, L, step=1):
    mask=(1<<site)+(1<<((site+step)%L))
    return state^mask

def flip_site(state, site):
    mask=(1<<site)
    return state^mask


def shift_left_pbc(state, L):
    return (state>>1)+((state&1)<<(L-1))

def fliplr(state, L):
    v=0
    j=0
    while j < L:
        v+=((state>>j)&1)<<(L-j-1)
        j+=1
    return v

def tobinary(state, L):
    buffer=''
    n=state+0
    j=L-1
    while j >= 0:
        buffer+=str(n%2)
        n/=2
        j-=1
    return buffer

def get_gs(H,num_gs=1):
    try:
        return scipy.sparse.linalg.eigsh(H,k=num_gs,which='SA') # = (E_gs,gs)
    except Exception as error:
        sys.stderr.write('Issues with convergence in function get_gs...\n')
        sys.stderr.write(str(error)+'\n')
        return None

