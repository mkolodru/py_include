import sys
import numpy

if len(sys.argv)<4:
	print('Proper usage: extract_col.py $colNum $infile $outfile')
	assert 1==2

colnum=int(sys.argv[1])
infile=sys.argv[2]
outfile=sys.argv[3]
out=open(outfile,'w')

for line in open(infile,'r'):
	pts=line.split()
	out.write(pts[0]+'\t'+pts[colnum]+'\t0\t0\t0\n')
