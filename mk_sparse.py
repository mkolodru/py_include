import numpy
import scipy.sparse as sp

def load_complex_sparse_matrix(f):
    pts=open(f,'r').readline().split()
    sz=(int(pts[0]),int(pts[1]))
    vals=numpy.loadtxt(f,skiprows=2)
    #     sz=int(max(max(vals[:,0]),max(val[:,1])))+1
    M=sp.lil_matrix(sz,dtype=complex)
    for row in range(vals.shape[0]):
        M[int(vals[row,0]),int(vals[row,1])]=complex(vals[row,2],vals[row,3])
    return M

def load_real_sparse_matrix(f):
    pts=open(f,'r').readline().split()
    sz=(int(pts[0]),int(pts[1]))
    vals=sp.lil_matrix(f,skiprows=2)
    #    sz=int(max(max(vals[:,0]),max(val[:,1])))+1
    M=numpy.zeros(sz)
    for row in range(vals.shape[0]):
        M[int(vals[row,0]),int(vals[row,1])]=vals[row,2]
    return M

def output_sparse(M,fstr):
    M2=M.tocsc()
    fout=open(fstr,'w')
    fout.write('row\tcol\tre(val)\tim(val)\n')
    (rows,cols)=M2.nonzero()
    for j in range(len(rows)):
        d=M2.data[j]
        out_str='%d\t%d\t%.19f\t%.19f\n' % (rows[j],cols[j],d.real,d.imag)
        fout.write(out_str)
    fout.close()
        
def print_sparse(M):
    M2=M.tocsc()
    (rows,cols)=M2.nonzero()
    for j in range(len(rows)):
        d=M2.data[j]
        out_str='%d\t%d\t%.19f\t%.19f' % (rows[j],cols[j],d.real,d.imag)
        print out_str
        
