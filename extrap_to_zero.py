import numpy
import sys
import mkfit

def find_linear_offset(xvals,yvals):
	assert len(xvals)==len(yvals)
	if len(xvals)==0:
		return (numpy.nan,numpy.nan)
	elif len(xvals)==1:
		return (numpy.nan,numpy.nan)
	elif len(xvals)==2:
		m=(yvals[1]-yvals[0])/(xvals[1]-xvals[0])
		b=yvals[0]-m*xvals[0]
		return (b,numpy.nan)
	else:
		(param,sparam)=mkfit.dofit(mkfit.linearfit,xvals,yvals)
		return (param[1],sparam[1])

def extrap_to_zero(data_lst,eps_lst):
	assert len(data_lst)==len(eps_lst)

	val=0*data_lst[0]
	sval=0*data_lst[0]

	xvals=numpy.zeros((len(data_lst)))
	yvals=numpy.zeros((len(data_lst)))

	if not isinstance(data_lst[0],numpy.ndarray):
		(val,sval)=find_linear_offset(numpy.array(eps_lst),numpy.array(data_lst))

	elif len(data_lst[0].shape)==1:
		row=0
		while row < data_lst[0].shape[0]:
			j=0
			while j < len(data_lst):
				xvals[j]=eps_lst[j]
				yvals[j]=data_lst[j][row]
				j+=1
			(val[row],sval[row])=find_linear_offset(xvals,yvals)
			row+=1
	else:
		row=0
		while row < data_lst[0].shape[0]:
			col=0
			while col < data_lst[0].shape[1]:
				j=0
				while j < len(data_lst):
					xvals[j]=eps_lst[j]
					yvals[j]=data_lst[j][row,col]
					j+=1
				(val[row,col],sval[row,col])=find_linear_offset(xvals,yvals)
				col+=1
			row+=1
		
		
	return (val,sval)
