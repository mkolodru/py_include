import sys
import numpy
import subprocess
import glob

def vcomp(a, b):
	ptsa=a.split('/')
	ptsb=b.split('/')
	va=int(ptsa[-3][1:])
	vb=int(ptsb[-3][1:])
	return va - vb
	

if len(sys.argv) < 4:
	print('Proper usage: getStatsIfDone.py $loc $gs_dir $acceptableError')
	sys.exit()

loc=sys.argv[1]
if loc[-1]!='/':
	loc=loc+'/'
gs_loc=sys.argv[2]
if gs_loc[-1]!='/':
	gs_loc=gs_loc+'/'
acceptableError=float(sys.argv[3])

files=sorted(glob.glob(loc+'v*/build/end_*.out'),cmp=vcomp)

nofinout=open('nofin.out','w')
errorstats=open('error_stats.dat','w')
energystats=open('energy_stats.dat','w')
Q_over_vLstats=open('Q_over_vL_stats.dat','w')
fidelitystats=open('fidelity_stats.dat','w')
F_scaledstats=open('F_scaled_stats.dat','w')
ndefstats=open('ndef_stats.dat','w')
nzstats=open('nz_stats.dat','w')
nz_gsstats=open('nz_gs_stats.dat','w')
ndef_scaledstats=open('ndef_scaled_stats.dat','w')
corrstats=range(100)
for corrnum in range(100):
	corrstats[corrnum]=open('corr_'+str(corrnum)+'_stats.dat','w')

vmap=numpy.loadtxt('versionmap.dat',skiprows=1)
vmap_labels=open('versionmap.dat','r').readline().split()
vel_col=vmap_labels.index('velocity')
L_col=vmap_labels.index('numSites')

for file in files:
	pts=file.split('/')
	vnum=int(pts[-3][1:])
	vmap_row=vnum-1
	assert abs(vmap[vmap_row,0]-vnum) < 0.1
	L=numpy.round(vmap[vmap_row,L_col])
	numSites=int(L)
	vel=vmap[vmap_row,vel_col]
	
	ind=file.find('end')
	assert ind!=-1
	stderrfile=file[:ind]+'error_'+str(vnum)+'.err'
	finished=False
	for line in open(stderrfile,'r'):
		if line.find('Final energy') != -1:
			finished=True
			break

	error_file=file.replace('end','error')
	for line in open(error_file,'r'):
		errorval=float(line)

	if finished:
		print('Version '+str(vnum)+' has finished')
		
		#Get observables the new way
		final_file=file #glob.glob('v'+str(vnum)+'/build/end_*.out')[0]
		cmd='../tMPS_caltech_getobs '+final_file+' '+gs_loc+'gs_'+str(int(L))+'_final.out '+str(vel)+' > temp.temp'
		print(cmd)
		subprocess.call(cmd,shell=True)

		obs_lst=[]
		val_lst=[]
		for line in open('temp.temp','r'):
			pts=line.split('=')
			obs_lst.append(pts[0])
			val_lst.append(float(pts[1]))


		energyval=val_lst[obs_lst.index('Energy')]
		Q=val_lst[obs_lst.index('Q')]
		Q_over_vL=val_lst[obs_lst.index('Q/vL')]
		F=val_lst[obs_lst.index('F')]
		F_sc=val_lst[obs_lst.index('F_sc')]
		ndef=val_lst[obs_lst.index('ndef')]
		nz=val_lst[obs_lst.index('nz')]
		nz_gs=val_lst[obs_lst.index('nz_gs')]
		ndef_sc=ndef/vel**0.5
		corr=[]
		for l in range(numSites/2+1):
			corr.append(val_lst[obs_lst.index('corr_'+str(l))])

		errorstats.write(str(vnum)+'\t'+str(errorval)+'\t0\t0\t0\n')
		if errorval <= acceptableError:
			energystats.write(str(vnum)+'\t'+str(energyval)+'\t0\t0\t0\n')
			Q_over_vLstats.write(str(vnum)+'\t'+str(Q_over_vL)+'\t0\t0\t0\n')
			fidelitystats.write(str(vnum)+'\t'+str(F)+'\t0\t0\t0\n')
			F_scaledstats.write(str(vnum)+'\t'+str(F_sc)+'\t0\t0\t0\n')
			ndefstats.write(str(vnum)+'\t'+str(ndef)+'\t0\t0\t0\n')
			nzstats.write(str(vnum)+'\t'+str(nz)+'\t0\t0\t0\n')
			nz_gsstats.write(str(vnum)+'\t'+str(nz_gs)+'\t0\t0\t0\n')
			ndef_scaledstats.write(str(vnum)+'\t'+str(ndef_sc)+'\t0\t0\t0\n')
			for l in range(len(corr)):
				corrstats[l].write(str(vnum)+'\t'+str(corr[l])+'\t0\t0\t0\n')
	else:
		print('Version '+str(vnum)+' has not finished')
		nofinout.write(str(vnum)+'\n')

files=glob.glob('*.qsub.o*')
noconv=[]

for file in files:
	for line in open(file,'r'):
		if line.find('SVD failed to converge') != -1:
			vnum=int(file.split('_')[-1].split('.')[0])
			noconv.append(vnum)
			break

noconv=sorted(noconv)

outfile=open('noconv.dat','w')
for val in noconv:
	outfile.write(str(val)+'\n')
