import numpy
import sys

def read_graph_data(label):
	print(label)
	infile=open('temp_data.dat','r')
	line=infile.readline()
	while len(line) > 0:
		if line.find(label)!=-1:
			break
		line=infile.readline()

	line=infile.readline()
	if len(line)==0:
		print('Unable to find the data you requested, label='+label)
		return None

	data=[]
	while line.find('***')==-1:
		if len(line)==0:
			print('We have a problem with reading: len=0')
			sys.exit()
		pts=line.split()
		data.append((float(pts[0]),float(pts[1]),float(pts[2]),int(pts[3])))
		line=infile.readline()

	return data
	
def read_graphs(labellst):
	data=[]
	print(labellst)
	for label in labellst:
		print(label)
		data.extend(read_graph_data(label))

	data=sorted(data,key=lambda val:val[0])
	xvals=numpy.zeros((len(data)))
	yvals=0*xvals
	svals=0*xvals
	vvals=numpy.zeros((len(data)),dtype=int)
	for j in range(len(data)):
		xvals[j]=data[j][0]
		yvals[j]=data[j][1]
		svals[j]=data[j][2]
		vvals[j]=data[j][3]

	return (xvals,yvals,svals,vvals)

def read_fit_data(label):
	infile=open('temp_data.dat','r')
	line=infile.readline()
	while len(line) > 0:
		if line.find(label)!=-1:
			break
		line=infile.readline()

	line=infile.readline()
	if len(line)==0:
		print('Unable to find the data you requested, label='+label)
		return None
	while line[1]=='=':
		line=infile.readline()

	param=[]
	sparam=[]
	while line.find('***')==-1:
		if len(line)==0:
			print('We have a problem with reading: len=0')
			sys.exit()
		pts=line.split()
		param.append(float(pts[0]))
		sparam.append(float(pts[1]))
		line=infile.readline()

	return (param,sparam)
