import numpy
import sys

if len(sys.argv) < 4:
	print('Proper usage: regen_stats.py $infile $outfile $regen_cnt')
	sys.exit()

regen_cnt=int(sys.argv[3])
xsum=numpy.zeros((regen_cnt))
xxsum=numpy.zeros((regen_cnt))
mean=numpy.zeros((regen_cnt))
std=numpy.zeros((regen_cnt))
tempx=numpy.zeros((regen_cnt))
npts=0
j=0
for line in open(sys.argv[1],'r'):
	tempx[j]=float(line)
	if j==regen_cnt-1:
		for j in range(regen_cnt):
			xsum[j]+=tempx[j]
			xxsum[j]+=tempx[j]**2
		j=-1
		npts+=1
	j+=1

out=open(sys.argv[2],'w')
for j in range(regen_cnt):
	mean[j]=xsum[j]/npts
	std[j]=numpy.sqrt(1.0/(npts-1.0)*(xxsum[j]-npts*(mean[j]**2)))
	out.write(str(mean[j])+'\t'+str(std[j])+'\n')
