import numpy
import os
try:
    assert len(os.environ['DISPLAY']) > 0
except:
    import matplotlib
    matplotlib.use('Agg')
import pylab
import mkplot
import matplotlib.font_manager
import mkfit
from read_graph_data import *
