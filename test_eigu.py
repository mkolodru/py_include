import numpy
from mk_qm import *
import numpy.random
import scipy.linalg

sz=20

def gen_rand_u(sz):
    H=numpy.random.random((sz,sz))+1.j*numpy.random.random((sz,sz))
    H+=adjoint(H)
    return scipy.linalg.expm(1.j*H)

# Test for a randomly generated unitary
U=gen_rand_u(sz)
(U_d,V)=eigu(U)
U_exp=V.dot(numpy.diag(U_d)).dot(adjoint(V))
diff=U-U_exp
print 'Max difference for random unitary is '+str(max(numpy.abs(diff.flatten())))

# Test for a randomly generated unitary with some phases = -phases
e1=U_d[sz/4-1]
e2=U_d[sz/2+1]
U*=numpy.conjugate(e1*e2)**0.5

(U_d,V)=eigu(U)
U_exp=V.dot(numpy.diag(U_d)).dot(adjoint(V))
diff=U-U_exp
print 'Max difference for matched unitary is '+str(max(numpy.abs(diff.flatten())))
