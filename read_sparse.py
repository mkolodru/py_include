import scipy.sparse
import numpy

def read_sparse(fin):
    data=numpy.loadtxt(fin,skiprows=1)
    sz=max(int(max(data[:,0])),int(max(data[:,1])))+1
    M=scipy.sparse.lil_matrix((sz,sz),dtype=complex)
    for j in range(data.shape[0]):
        M[int(data[j,0]),int(data[j,1])]=data[j,2]+1.j*data[j,3]
    return M.tocsc()
