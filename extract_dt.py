import sys
import glob
import numpy

def vcmp(a,b):
	na=int(a.split('/')[0][1:])
	nb=int(b.split('/')[0][1:])
	return na-nb

fileList=glob.glob('v*/build/dt_*.out')
#sort by version number
fileList=sorted(fileList,cmp=vcmp)
out=open('dt_stats.dat','w')

for file in fileList:
	vnum=int(file.split('/')[0][1:])
	dt=float(numpy.loadtxt(file)[1]) # Time per AVER_CNT
	out.write(str(vnum)+'\t'+str(dt)+'\t0\t0\t0\n')
