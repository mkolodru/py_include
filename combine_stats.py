import sys
import numpy

if len(sys.argv) < 4:
    print("Proper usage: combineStats.py $infile $outfile $numproccessorsper")
    assert 1==2

infile=open(sys.argv[1],'r')
outfile=open(sys.argv[2],'w')
np=int(sys.argv[3])
x=numpy.zeros((np))
linenum=1;
for line in infile:
    pts=line.split()
    x[linenum-1]=float(pts[1])
    if linenum % np == 0:
        xsq=x*x
        mean=sum(x)/np
        var=sum(xsq)/np-mean**2
        std=numpy.sqrt(var/(np-1))
        outfile.write(str(pts[0])+'\t'+str(mean)+'\t'+str(var)+'\t'+str(std)+'\t0\n')
        linenum=0
    linenum+=1
