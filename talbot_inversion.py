# Matlab code for Talbot numerical inversion of a Laplace transform
# copied from http://www.mathworks.com/matlabcentral/fileexchange/
# 39035-numerical-inverse-laplace-transform/content/talbot_inversion.m

import numpy
import sys

def cot(x):
    return 1./numpy.tan(x)

def talbot_inversion(f_s, t, M=64):
    # ilt = talbot_inversion(f_s, t, [M])
    #
    # Returns an approximation to the inverse Laplace transform of function
    # handle f_s evaluated at each value in t (1xn) using Talbot's method as
    # summarized in the source below.
    # 
    # This implementation is very coarse; use talbot_inversion_sym for better
    # precision. Further, please see example_inversions.m for discussion.
    #
    # f_s: Handle to function of s
    # t:   Times at which to evaluate the inverse Laplace transformation of f_s
    # M:   Optional, number of terms to sum for each t (64 is a good guess);
    #      highly oscillatory functions require higher M, but this can grow
    #      unstable; see test_talbot.m for an example of stability.
    # 
    # Abate, Joseph, and Ward Whitt. "A Unified Framework for Numerically 
    # Inverting Laplace Transforms." INFORMS Journal of Computing, vol. 18.4 
    # (2006): 408-421. Print.
    # 
    # The paper is also online: http://www.columbia.edu/~ww2040/allpapers.html.
    # 
    # Tucker McClure
    # Copyright 2012, The MathWorks, Inc.
    
    # Make sure t is n-by-1.
    if len(t.shape)!=1:
        sys.stderr.write('Input times, t, must be a vector\n')
        sys.exit()
        #    if size(t, 1) == 1
        #    t = t';
        #    elseif size(t, 2) > 1
        #        error('Input times, t, must be a vector.');
        #    end

    # Vectorized Talbot's algorithm
    k=numpy.arange(1,M)
    #    k = 1:(M-1); # Iteration index
    
    # Calculate delta for every index.
    delta=numpy.zeros((M),dtype=complex)
    delta[0]=2.*M/5.
    delta[1:]=2.*numpy.pi/5.*k*(cot(numpy.pi*k/float(M))+1.j)
    numpy.savetxt('delta.out',delta)
    
    # Calculate gamma for every index.
    gamma=numpy.zeros((M),dtype=complex)
    gamma[0]=0.5*numpy.exp(delta[0])
    gamma[1:]=(1.+1.j*numpy.pi/float(M)*k*(1.+cot(numpy.pi/float(M)*k)**2)
               -1.j*cot(numpy.pi/float(M)*k))*numpy.exp(delta[1:])
    numpy.savetxt('gamma.out',gamma)
    
    # Make a mesh so we can do this entire calculation across all k for all
    # given times without a single loop (it's faster this way).
    (delta_mesh,t_mesh) = numpy.meshgrid(delta, t)
    (gamma_mesh,t_mesh) = numpy.meshgrid(gamma, t)

    # Finally, calculate the inverse Laplace transform for each given time.
    ilt=numpy.zeros((len(t)))
    assert len(t)==t_mesh.shape[0]
    j=0
    while j < t_mesh.shape[0]:
        #        print 'j='+str(j)
        l=0
        while l < t_mesh.shape[1]:
            ilt[j]+=numpy.real(gamma_mesh[j,l]*f_s(delta_mesh[j,l]/t_mesh[j,l]))
            #            print str(gamma_mesh[j,l])+','+str(f_s(delta_mesh[j,l]/t_mesh[j,l]))
            l+=1
        ilt[j]*=(0.4/t[j])
        j+=1
    return ilt

