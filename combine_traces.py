import sys
import numpy
import glob
import pylab

if len(sys.argv) < 4:
	print('Proper usage: combine_traces.py $first $last $max_steps')
	sys.exit()

vfirst=int(sys.argv[1])
vlast=int(sys.argv[2])
max_steps=int(sys.argv[3])
template=glob.glob('v'+str(vfirst)+'/build/error_*.out')[0]
elen=len(template.split('_')[0])
template=template[(elen+1):]
filelist=glob.glob('v'+str(vfirst)+'/build/*_'+template)

for j in range(len(filelist)):
	filelist[j]=filelist[j].split('/')[-1]
	filelist[j]=filelist[j][:-len(template)]

nr = numpy.zeros((max_steps))
nr_set = False
for j in range(len(filelist)):
	if filelist[j].find('h')==-1:
		print(filelist[j])
		data=numpy.zeros((max_steps))
		for v in range(vfirst,vlast+1):
			file='v'+str(v)+'/build/'+filelist[j]+template
			data2=numpy.loadtxt(file)
			k=0
			while k + max_steps <= len(data2):
				data[:]+=data2[k:(k+max_steps)]
				if not nr_set:
					nr+=1.0
				k+=max_steps
			nleft=len(data2)-k
			if nleft > 0:
				data[:nleft]+=data2[k:]
				if not nr_set:
					nr[:nleft]+=1.0

		data = data / nr
		outfile='v'+str(vfirst)+'/build/'+filelist[j]+'combined_'+template
		numpy.savetxt(outfile,data)
		nr_set=True

pylab.plot(nr)
pylab.show()
