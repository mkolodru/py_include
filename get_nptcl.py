import numpy

def get_nptcl(nFermi2):
	mapping=numpy.loadtxt('/group/pcts/mkolodru/stochDiag/lookupTables/nFermi2toNptcl.dat',dtype=int)
	j=0;
	while mapping[j,0] < nFermi2:
		j+=1;
	if mapping[j,0] > nFermi2:
		j-=1
	return mapping[j,1]

