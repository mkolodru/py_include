import numpy
import pylab
import curve_fit
import mkfit
from operator import itemgetter
import sys

def get_all_vals(col_name,vmap_file='./versionmap.dat',mydtype=float):
	col=open(vmap_file,'r').readline().split().index(col_name)
	vmap=numpy.array(numpy.loadtxt(vmap_file,skiprows=1)[:,col],dtype=mydtype)
	lst=[]
	j=0
	while j < len(vmap):
		if vmap[j] not in lst:
			lst.append(vmap[j])
		j+=1
	return sorted(lst)

def set_tick_size(ax,fontsz,which='xy'):
	if which.find('x')!=-1:
		for tick in ax.xaxis.get_major_ticks():
			tick.label.set_fontsize(fontsz)
	if which.find('y')!=-1:
		for tick in ax.yaxis.get_major_ticks():
			tick.label.set_fontsize(fontsz)
				

def doplot(ax,xvals,yvals,sigvals,title,fitfn=None,fitparams=None,**kwargs):
	if len(xvals)==0:
		return
	
	ax.errorbar(xvals,yvals,sigvals,label=title,**kwargs)
	if fitfn != None:
		nonzero=True
		if sigvals != None:
			for j in range(len(sigvals)):
				if not (sigvals[j] > 0 and sigvals[j] != numpy.inf):
					nonzero=False

		if not nonzero:
			sigvals=None
			print('Fitting without error bars')

		try:
			(param,sparam)=mkfit.dofit(fitfn,xvals,yvals,sigma=sigvals,p0=fitparams)
			print('Fitting to data set: '+title)
			mkfit.print_fit(fitfn,param,sparam,sys.stdout)
			pvals=mkfit.getprintvals(fitfn)
			fitlabel='Fit,'+title+':'+pvals[0]
			for j in range(len(param)):
				fitlabel+='\n'+pvals[1][j]+'='+str(param[j])+'+-'+str(sparam[j])
			x1=min(xvals)
			x2=max(xvals)
			diff=x2-x1
			x3=x1-diff
			x4=x2+diff
			numvals=1000.0
			xfit=numpy.arange(x3,x4,3*diff/numvals)
			yfit=fitfn(xfit,*param)
			ax.plot(xfit,yfit,'-',label=fitlabel)
			outfile=open('temp_data.dat','a')
			outfile.write(fitlabel+'\n')
			for j in range(len(param)):
				outfile.write(str(param[j])+'\t'+str(sparam[j])+'\n')
			outfile.write('**********\n')
			outfile.close()

		except RuntimeError:
			print('Unable to converge the fit')


def mkplot(ax,xind,ystr,fixedCols,fixedVals,title,folder='./',fitfn=None,fitparams=None,xr=None,**kwargs):
	if folder[-1] != '/':
		folder+='/'
	versionmap=numpy.loadtxt(folder+"versionmap.dat",skiprows=1)
	ydata=numpy.loadtxt(folder+ystr+"_stats.dat")
	assert len(fixedCols)==len(fixedVals)
	nfixed=len(fixedCols)
	ind=[]
	ind12_versions=[]
	for i in range(versionmap.shape[0]):
		j=0
		while j < nfixed:
			if versionmap[i,fixedCols[j]]!=fixedVals[j]:
				break
			j+=1
		if j==nfixed:
			ind.append(i)
			ind12_versions.append(int(numpy.round(versionmap[i,0])))

	#	ind12_versions=[int(i) for i in	 versionmap[ind,0].copy()]
	#	yind=[i for i in range(len(ydata[:,0])) if ydata[i,0] in ind12_versions]
	#	vmap_ind=[int(i) for i in list(ydata[yind,0]-1)]

	yind=sorted([i for i in range(len(ydata[:,0])) if ydata[i,0] in ind12_versions])
	vmap_ind=[]
	yloc=0
	vmaploc=0
	while vmaploc < versionmap.shape[0] and yloc < len(yind):
		if abs(versionmap[vmaploc,0]-ydata[yind[yloc],0]) < 0.1:
			vmap_ind.append(vmaploc)
			yloc+=1
		vmaploc+=1

	if xr!=None:
		assert(len(xr)==2)
		#Cut the vmap_ind down to size
		vmap_ind=[i for i in vmap_ind if (versionmap[i,xind]>=xr[0] and versionmap[i,xind]<=xr[1])]
		#Then remake yind
		yind=[]
		yloc=0
		vmaploc=0
		while vmaploc < len(vmap_ind) and yloc < ydata.shape[0] :
			if abs(versionmap[vmap_ind[vmaploc],0]-ydata[yloc,0]) < 0.1:
				yind.append(yloc)
				vmaploc+=1
			yloc+=1

	vvals=versionmap[vmap_ind,0]
	xvals=versionmap[vmap_ind,xind]
	yvals=ydata[yind,1]
	svals=ydata[yind,3]
	slist=[]
	for j in range(len(xvals)):
		slist.append((xvals[j],yvals[j],svals[j],vvals[j]))
	slist=sorted(slist,key=itemgetter(0))
	for j in range(len(xvals)):
		xvals[j]=slist[j][0]
		yvals[j]=slist[j][1]
		svals[j]=slist[j][2]
		vvals[j]=slist[j][3]

	outfile=open('temp_data.dat','a')
	outfile.write(title+'\n')
	for j in range(len(xvals)):
		outfile.write(str(xvals[j])+'\t'+str(yvals[j])+'\t'+str(svals[j])+'\t'+str(int(numpy.round(vvals[j])))+'\n')
	outfile.write('**********\n')
	outfile.close()
	
	doplot(ax,xvals,yvals,svals,title,fitfn=fitfn,fitparams=fitparams,**kwargs)
		
		
def plotseries(ax,xstr,ystr,slowMatrix,folder='./',legendprefix='',fitfn=None,fitparams=None,xr=None,appenddata=False,**kwargs):
	if not appenddata:
		outfile=open('temp_data.dat','w') #Clear the file
		outfile.close()
	if folder[-1] != '/':
		folder+='/'
	if legendprefix!=None and len(legendprefix)==0:
		legendprefix=ystr
	infile=open(folder+"versionmap.dat",'r')
	firstline=infile.readline()
	labels=firstline.split()
	ncols=len(labels)
	infile.close()
	numslow=len(slowMatrix)
	assert numslow<=ncols-1
	slowSet=[]
	temp=range(numslow)
	loopall(slowMatrix,slowSet,temp,0)
	fixedCols=range(numslow)
	for j in range(numslow):
		assert labels.count(slowMatrix[j][0]) > 0
		fixedCols[j]=labels.index(slowMatrix[j][0])
	for fixedVals in slowSet:
		if legendprefix==None:
			title='_no_legend'
		else:
			title=legendprefix
			for j in range(numslow):
				if len(slowMatrix[j][1]) > 1:
					title+=', '+labels[fixedCols[j]]+'='+str(fixedVals[j])
		mkplot(ax,labels.index(xstr),ystr,fixedCols,fixedVals,title,folder=folder,fitfn=fitfn,fitparams=fitparams,xr=xr,**kwargs)
		
def loopall(slowMatrix,slowSet,temp,ind):
	if ind==len(temp):
		slowSet.append(list(temp))
	else:
		for i in slowMatrix[ind][1]:
			temp[ind]=i
			loopall(slowMatrix,slowSet,temp,ind+1)
	
# Similar to numpy meshgrid, but shifted and bigger so that it will render the
# pcolor graph properly.  Assumes that x and y are evenly spaced arrays.
# Returns arrays of size (len(x)+1, len(y)+1)
def mk_meshgrid(y,x):
	dy=(y[-1]-y[0])/(float(len(y))-1.)
	y2=y-dy/2.
	y2=numpy.append(y2,y2[-1]+dy)
	dx=(x[-1]-x[0])/(float(len(x))-1.)
	x2=x-dx/2.
	x2=numpy.append(x2,x2[-1]+dx)
	
	return numpy.meshgrid(y2,x2)
