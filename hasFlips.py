import glob
import string
import numpy

files=glob.glob("v*/build/Eden_*.out");
cutvals=numpy.loadtxt('stats_section.dat')
for f in files:
    vstr=f.split('/')[0][1:]
    cutval=int(cutvals[int(vstr)-1][1]);
    linenum=0
    for line in open(f,'r'):
        d1=float(line);
        if linenum > cutval and d1 < 0:
            print(vstr)
            break
        linenum+=1
            
        
