import numpy;

versionmap=numpy.loadtxt('versionmap.dat',skiprows=1);
fin=open('versionmap.dat','r');
labels=fin.readline().split()
fin.close()
nFermi2col=labels.index('nFermi2');
mapping=numpy.loadtxt('../../lookupTables/nFermi2toNptcl.dat');

outfile=open('Nptcl_col.dat','w');

for nFermi2 in versionmap[:,nFermi2col]:
	j=0;
	while mapping[j,0] < nFermi2:
		j+=1;
	if mapping[j,0] > nFermi2:
		j-=1
	outfile.write(str(int(mapping[j,1]))+'\n');

outfile.close();

